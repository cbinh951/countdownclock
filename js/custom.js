$(document).ready(function(){
    $("#add").click(function(){
        $("#items").append('<div><input type="text" class="input_number form-control" name="number"></div>');
    });
})

function countdown(number, callback, name){
    number = number == '' ? 60 : number;
    var x = setInterval(function() {
        number--;      
        var seconds = Math.floor( number % 60 );
        var minutes = Math.floor( number/60 % 60 );
        var hours = Math.floor( (number/(60*60)) % 24 );

        console.log(name + " - " + hours + " : "+ minutes + " : " + seconds);
     
        if (number == 0) {
            clearInterval(x);
            excute(name);
        }
    }, 1000);
}

function excute(name){
    console.log('Exprired ' + name);
}

function startTime(){
    var names = document.getElementsByName('number');
    for(key = 0; key < names.length; key++)  {
        var number = key;
        number = names.length == 1 ? '' : ++number;
        countdown(names[key].value, excute, 'Countdown ' + number);
    }
}
